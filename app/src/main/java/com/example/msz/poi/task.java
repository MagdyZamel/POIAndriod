package com.example.msz.poi;

import android.os.AsyncTask;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class task extends AsyncTask<String, Void, String> {

    CallBack delegate;
        @Override
        protected String doInBackground(String... params) {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(params[0])
                    .build();

            try {
                Response response = client.newCall(request).execute();
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }





    @Override
        protected void onPostExecute(String result) {
            final Gson gson = new Gson();
            MovieRespons  ob = gson.fromJson(result.toString(), MovieRespons.class);
            call.methodToCallBack(ob);


    }


}
